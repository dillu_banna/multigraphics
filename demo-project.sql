-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 09, 2021 at 11:27 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo-project`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2021_02_08_112211_create_categories_table', 1),
(10, '2021_02_09_072759_create_email_templates_table', 1),
(11, '2021_02_10_055349_create_content_management_table', 1),
(12, '2021_02_10_070428_create_global_setting_management_table', 1),
(13, '2021_02_17_053801_create_states_table', 1),
(14, '2021_02_17_053818_create_countries_table', 1),
(15, '2021_02_17_053828_create_cities_table', 1),
(17, '2021_02_22_102126_create_sub_category_management_table', 1),
(18, '2021_02_27_093701_create_banners_table', 1),
(19, '2021_03_02_065755_create_introduction_screen_contents_table', 1),
(20, '2021_03_24_054958_create_permissions_table', 1),
(21, '2021_04_01_064525_create_products_table', 1),
(22, '2021_04_01_064621_create_wishlists_table', 1),
(23, '2021_04_01_064726_create_comments_table', 1),
(24, '2021_04_01_100758_create_faq_management_table', 1),
(25, '2021_04_01_112953_create_permission_modules_table', 1),
(26, '2021_04_02_071806_create_contact_us_table', 1),
(27, '2021_04_03_052816_create_shipping_addresses_table', 1),
(28, '2021_04_03_053140_create_billing_addresses_table', 1),
(29, '2021_04_03_073936_create_product_images_table', 1),
(30, '2021_04_06_070524_create_new_addresses_table', 1),
(31, '2021_04_07_055727_create_cards_table', 1),
(32, '2021_04_07_061151_create_push_notification_settings_table', 1),
(33, '2021_04_08_073754_create_rate_and_reviews_table', 1),
(34, '2014_10_12_000000_create_users_table', 2),
(35, '2021_02_17_053839_create_user_management_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_deleted` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0 => deleted, 1 =>is_deleted',
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `role` tinyint(4) NOT NULL DEFAULT 2 COMMENT '1 => admin, 2 => sub admin',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `image`, `mobile_number`, `is_deleted`, `updated_by`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'styl@getnada.com', NULL, '$2y$10$UCLOMU2.RjX34ahPOn5VB.wiAbl9POuwgj39mo8nAlla4wSwX1WvW', '4X3evZGKFlSlKDqItEsrNnklNhO0jhxkhIxeRywq.jpg', NULL, 1, NULL, 1, NULL, '2021-04-08 05:39:57', '2021-04-08 05:39:57');

-- --------------------------------------------------------

--
-- Table structure for table `user_management`
--

CREATE TABLE `user_management` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1 => active, 0 => inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_management`
--

INSERT INTO `user_management` (`id`, `full_name`, `phone_number`, `email`, `image`, `dob`, `password`, `status`, `created_at`, `updated_at`) VALUES
(2, 'dsa', '432432423', 'ds@gh.gdfg', '1617944632.jpg', '04/02/2021', '$2y$10$/sTpdrd1wXn5fiJLTEX3aefzBXf3txdoy4SkrKNhH9RhnmWC7hNqy', 1, '2021-04-08 23:33:52', '2021-04-08 23:33:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `user_management`
--
ALTER TABLE `user_management`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_management_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_management`
--
ALTER TABLE `user_management`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
