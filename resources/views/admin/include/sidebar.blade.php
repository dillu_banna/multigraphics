<!-- partial:partials/_settings-panel.html -->
{{--<div id="settings-trigger"><i class="icon-settings"></i></div>--}}
<div id="theme-settings" class="settings-panel">
    <i class="settings-close icon-close"></i>
    <p class="settings-heading">SIDEBAR SKINS</p>
    <div class="sidebar-bg-options selected" id="sidebar-default-theme">
        <div class="img-ss rounded-circle bg-dark border mr-3"></div>Default
    </div>
    <div class="sidebar-bg-options" id="sidebar-light-theme">
        <div class="img-ss rounded-circle bg-light border mr-3"></div>Light
    </div>
    <p class="settings-heading mt-2">HEADER SKINS</p>
    <div class="color-tiles mx-0 px-4">
        <div class="tiles dark"></div>
        <div class="tiles default light"></div>
    </div>
</div>
<!-- partial -->
<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
                <div class="profile-image">
                    @if(auth('admin')->user()->image)
                    <img class="img-xs rounded-circle" src="{{asset('public/storage/uploads/users/'. auth('admin')->user()->image ?? '')}}" alt="profile image">
                    @else
                        <img class="img-xs rounded-circle" src="{{asset('public/storage/uploads/users/avtar.png')}}" alt="profile image">
                    @endif
                </div>
                <div class="text-wrapper">
                    <p class="profile-name">{{ \Illuminate\Support\Str::limit(ucfirst(auth('admin')->user()->name ?? ''), 15, $end='...')}}</p>
                    <p class="designation">{{auth('admin')->user()->email ?? ''}}</p>
                </div>
            </a>
        </li>
        <li class="nav-item nav-category"><a class="nav-link" href="{{route('admin.dashboard')}}">Dashboard</a></li>
        @php
            $admin_menu_active = Request::segment('2');
        @endphp

        <li class="nav-item ">
            
                <a class="nav-link {{$admin_menu_active ? ""  :"collapsed"}}" data-toggle="collapse" href="#" aria-expanded="{{$admin_menu_active ? "true" : "false"}}" aria-controls="ui-basic">
                    <span class="menu-title">Admin</span>
                    <i class="fa fa-user-circle-o menu-icon"></i>
                </a>
           
                <div class="collapse {{$admin_menu_active ? "show" : ""}}" id="ui-basic">
                    <ul class="nav flex-column sub-menu">
                        <li class="nav-item"> <a class="nav-link {{$admin_menu_active == 'user-management'? 'active' : 'unactive'}}" href="{{route('admin.user.list')}}">Employee Management</a></li>
                        
                    </ul>
                </div>
            </li>

            
        </ul>
</nav>

