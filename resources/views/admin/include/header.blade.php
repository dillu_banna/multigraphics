<!-- partial:partials/_navbar.html -->
<nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
        <a class="navbar-brand brand-logo" href="{{route('admin.dashboard')}}">
            <img src="{{asset('public/storage/uploads/project-logo/stylhawklogo.png')}}" alt="logo" class="logo-dark"/>
        </a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center">
        <h5 class="mb-0 font-weight-medium d-none d-lg-flex">{{$title ?? ''}}</h5>
        <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item dropdown d-none d-xl-inline-flex user-dropdown">
                <a class="nav-link" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                    @if(auth('admin')->user()->image)
                        <img class="img-xs rounded-circle ml-2" src="{{asset('public/storage/uploads/users/'. auth('admin')->user()->image ?? 'avtar.png')}}" alt="Profile image"> </a>
                    @else
                        <img class="img-xs rounded-circle ml-2" src="{{asset('public/storage/uploads/users/avtar.png')}}" alt="Profile image"> </a>
                    @endif
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                    <div class="dropdown-header text-center">
                        @if(auth('admin')->user()->image)
                            <img class="img-md rounded-circle" src="{{asset('public/storage/uploads/users/'. auth('admin')->user()->image ?? '')}}" alt="Profile image" style="height: 50px; width:50px">
                        @else
                            <img class="img-xs rounded-circle ml-2" src="{{asset('public/storage/uploads/users/avtar.png')}}" alt="Profile image"> </a>
                        @endif
                        <p class="mb-1 mt-3">{{ \Illuminate\Support\Str::limit(ucfirst(auth('admin')->user()->name ?? ''), 15, $end='...')}}</p>
                        <p class="font-weight-light text-muted mb-0">{{auth('admin')->user()->email ?? ''}}</p>
                    </div>
                    <a class="dropdown-item" href="{{route('admin.profile.edit.show')}}"><i class="dropdown-item-icon fa fa-user text-primary"></i> My Profile</a>
                    <a class="dropdown-item" href="{{route('admin.show.change.password')}}"><i class="dropdown-item-icon fa fa-unlock text-primary"></i>Change Password</a>
                    @guest
                    <a class="dropdown-item" href="{{route('admin.login.show')}}"><i class="dropdown-item-icon icon-powerfa fa-sign-in text-primary"></i>Login</a>
                        @if (Route::has('register'))
                            <a class="dropdown-item" href="{{route('register')}}"><i class="dropdown-item-icon icon-power text-primary"></i>Register</a>
                        @endif
                        @else
                        <a class="dropdown-item" href="{{ route('admin.logout') }}" onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                            <i class="dropdown-item-icon fa fa-sign-out text-primary"></i>Logout
                                <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                        </a>
                    @endguest
                </div>
            </li>
        </ul>
    </div>
</nav>
<!-- partial -->
