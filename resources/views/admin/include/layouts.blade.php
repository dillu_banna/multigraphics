<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Style Hawk</title>
    @toastr_css
    <!-- plugins:css -->

    <link rel="stylesheet" href="{{asset('public/assets/vendors/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/assets/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('public/assets/vendors/jvectormap/jquery-jvectormap.css')}}">
    <link rel="stylesheet" href="{{asset('public/assets/vendors/daterangepicker/daterangepicker.css')}}">
{{--    <link rel="stylesheet" href="{{asset('public/assets/vendors/chartist/chartist.min.css')}}">--}}

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.5.5/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="{{asset('public/assets/css/demo_1/style.css')}}">
    <link rel="shortcut icon" href="{{asset('public/assets/images/favicon.png')}}" />
    <link rel="stylesheet" href="{{asset('public/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
    <style>
        .error
        {
            color: red;
        }
        .dataTables_wrapper .dataTables_length select {
            height: auto;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            margin: -16px;
        }
        .select2-container--default .select2-selection--single {
            padding-top: 20px;

        }

        #all-scroll {cursor: all-scroll;}

    </style>
</head>
<body>
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    @include('admin.include.header')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_settings-panel.html -->
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        @include('admin.include.sidebar')
        <!-- partial -->
        <div class="main-panel">
            @jquery
            @toastr_js
            @toastr_render
            @yield('content')
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            @include('admin.include.footer')
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{asset('public/assets/vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{asset('public/assets/vendors/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('public/assets/vendors/jvectormap/jquery-jvectormap.min.js')}}"></script>
<script src="{{asset('public/assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>


<script src="{{asset('public/assets/vendors/moment/moment.min.js')}}"></script>
<script src="{{asset('public/assets/vendors/daterangepicker/daterangepicker.js')}}"></script>

{{--<script src="{{asset('public/assets/vendors/chartist/chartist.min.js')}}"></script>--}}
<script src="{{asset('public/assets/vendors/progressbar.js/progressbar.min.js')}}"></script>
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{asset('public/assets/js/off-canvas.js')}}"></script>
<script src="{{asset('public/assets/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('public/assets/js/misc.js')}}"></script>
<script src="{{asset('public/assets/js/settings.js')}}"></script>
<script src="{{asset('public/assets/js/todolist.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page -->
{{--<script src="{{asset('public/assets/js/dashboard.js')}}"></script>--}}


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>

<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.5.9/adapters/jquery.js"></script>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css">

<script src="{{asset('public/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>

@yield('scripts')
</body>
</html>
