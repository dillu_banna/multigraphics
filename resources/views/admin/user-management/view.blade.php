@extends('admin.include.layouts')
@section('content')
    <div class="content-wrapper">
        <div class="row grid-margin">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="get" action="{{route('admin.user.view', ['id'=>base64_encode($user_view->id), 'page'=>request('page')])}}">
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">First Name</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->first_name ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">Last Name</label>
                                </div>
                                <div class="col-lg-9">
                                    <label class="col-form-label">{{ucfirst($user_view->last_name ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">User Name</label>
                                </div>
                                <div class="col-lg-9">
                                    <label class="col-form-label">{{$user_view->user_name ?? ''}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">Phone Number</label>
                                </div>
                                <div class="col-lg-9">
                                    <label class="col-form-label">{{$user_view->phone_number ?? ''}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">Email</label>
                                </div>
                                <div class="col-lg-9">
                                    <label class="col-form-label">{{$user_view->email ?? ''}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">Status</label>
                                </div>
                                <div class="col-lg-9">
                                    <label class="col-form-label">{{$user_view->status == 1 ? 'Active' : 'Inactive'}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: 7px; padding: 5px">
                                <strong><h5>Shipping Address</h5></strong>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">First Name</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->shippingAddress->shipping_first_name ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">Last Name</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->shippingAddress->shipping_last_name ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">Shipping Address</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->shippingAddress->shipping_address ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">State</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->shippingAddress->states->name ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">City</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->shippingAddress->cities->name ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">Zip Code</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->shippingAddress->zipcode ?? '')}}</label>
                                </div>
                            </div>

                            <div class="form-group row" style="margin: 7px; padding: 5px">
                                <strong><h5>Billing Address</h5></strong>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">First Name</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->billingAddress->billing_first_name ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">Last Name</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->billingAddress->billing_last_name ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">Shipping Address</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->billingAddress->billing_address ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">State</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->billingAddress->states->name ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">City</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->billingAddress->cities->name ?? '')}}</label>
                                </div>
                            </div>
                            <div class="form-group row" style="margin: -18px">
                                <div class="col-lg-3">
                                    <label class="col-form-label">Zip Code</label>
                                </div>
                                <div class="col-lg-3">
                                    <label class="col-form-label">{{ucfirst($user_view->billingAddress->zipcode ?? '')}}</label>
                                </div>
                            </div>
                           </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="text-align: center">
            <a class="btn btn-success btn-fw" href="{{url('admin/user-management/list?page='.request('page'))}}"><i class="fa fa-backward"></i>  Back</a>
        </div>
    </div>
@endsection


