@extends('admin.include.layouts')
@section('content')

<div class="content-wrapper">
    <div class="row">
            <div class="col-lg-12">
        <div class="col-2 grid-margin stretch-card">

            <a class="btn btn-success btn-fw" href="{{route('admin.user.add.view')}}"><i class="fa fa-plus"></i>  Create</a>

        </div>
        <form action="{{route('admin.user.list')}}" method="get">

            <div class="col-8 grid-margin stretch-card">
                <input type="text" name="full_name" placeholder="Full Name" id="full_name" value="{{ request('full_name') }}">
                <input type="text" name="email" placeholder="Email" id="email" value="{{ request('email') }}">
                <input type="text" name="phone_number" placeholder="Phone Number" id="phone_number" value="{{ request('phone_number') }}">
                
            </div>
            <div class="col-8 grid-margin stretch-card">
                <input type="text" id="from" name="from" value="{{request('from') ?? ''}}" placeholder="From Date">&nbsp;&nbsp;
                <input type="text" id="to" name="to" value="{{request('to') ?? ''}}" placeholder="To Date"> &nbsp;&nbsp;
                
                <select name="status" id="status">
                  <option value="">Select status {{request('status')}}</option>
                  <option value="1" {{request('status') == 1 ? 'selected' : ''}}>Active</option>
                  <option value="0" {{request('status') == 0 ? 'selected' : ''}}>InActive</option>
                </select>
                <br><br>
                &nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-success btn-sm">Filter</button> &nbsp;&nbsp;
                <a href="{{ route('admin.user.list') }}"><button type="button" id="reset" class="btn btn-primary btn-sm">Reset</button></a>
            </div>
        </form>
    </div>
</div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-12">
                    <div class="table-responsive">
                        <table id="data-table-id" class="table">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Full Name</th>
                                    <th>Phone Number</th>
                                    <th>Email</th>
                                    <th>Date</th>
                                    <th>Image</th>
                                    <th>DOB</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($user_list as $key => $user_data)
                                <tr>
                                    <td>{{$user_list->firstItem() + $key}}</td>
                                    <td>{{ucfirst($user_data->full_name)}}</td>
                                    <td>{{$user_data->phone_number ?? ''}}</td>
                                    <td>{{$user_data->email ?? ''}}</td>
                                    <td>{{date_format($user_data->created_at,'d-m-y' ?? '')}}</td>
                                    <td>
                                        @if($user_data->image)
                                            <img src="{{asset('public/storage/uploads/user-management/'.$user_data->image ?? '')}}">
                                        @else
                                            <img src="{{asset('public/storage/uploads/user-management/avtar.png')}}">
                                        @endif
                                    </td>
                                    <td>{{$user_data->dob ?? ''}}</td>
                                    <td>
                                        <label class="badge {{$user_data->status == 1 ? 'badge-success' : 'badge-danger'}}">{{$user_data->status == 1 ? 'Active' : 'Inactive'}}</label>
                                    </td>

                                    <td>
                                        <a class="btn btn-outline-success" href="{{route('admin.user.edit.view', ['id'=>base64_encode($user_data->id), 'page'=>request('page')])}}"> <i class="fa fa-pencil-square-o"></i>
                                                        Edit</a>
                                        <a class="btn btn-outline-danger" onclick="return confirm('Are you sure?')" href="{{route('admin.user.delete', ['id'=>base64_encode($user_data->id), 'page'=>request('page')])}}"> <i class="fa fa-trash"></i>
                                          Delete</a>
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                            {{$user_list->links()}}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready( function () {
    var uri = window.location.toString();
    if (uri.indexOf("?") > 0) {
        var clean_uri = uri.substring(0, uri.indexOf("?"));
        window.history.replaceState({}, document.title, clean_uri);
    }

    $('#data-table-id').DataTable({
        "paging": false,
        columnDefs: [
            { orderable: false, targets:8 },
        ]
    });



    $('#from').datepicker({
        uiLibrary: 'bootstrap4'
    });

    $('#to').datepicker({
        uiLibrary: 'bootstrap4'
    });



});
</script>
@endsection
