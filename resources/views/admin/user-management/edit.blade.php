@extends('admin.include.layouts')
@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <form class="cmxform" id="user-management-edit-form" method="post" action="{{route('admin.user.edit', ['id'=>base64_encode($edit->id), 'page'=>request('page')])}}" autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            <fieldset>
                                <div class="form-group">
                                    <label for="first_name">Full Name
                                        <span class="required-asterisk" style="color: red">*</span>
                                    </label>
                                    <input id="first_name" class="form-control @error('full_name') is-invalid @enderror" value="{{$edit->full_name ?? ''}}" name="full_name" type="text" placeholder="Full Name">
                                    @error('full_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="email">Email
                                        <span class="required-asterisk" style="color: red">*</span>
                                    </label>
                                    <input id="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$edit->email ?? ''}}" type="text" placeholder="Email Address">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="phone_number">Phone Number
                                        <span class="required-asterisk" style="color: red">*</span>
                                    </label>
                                    <input id="phone_number" class="form-control @error('phone_number') is-invalid @enderror" value="{{$edit->phone_number ?? ''}}" name="phone_number" type="text" placeholder="Phone Number">
                                    @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="location">Date Of Birth
                                        <span class="required-asterisk" style="color: red">*</span>
                                    </label>
                                    <input id="dob" class="form-control @error('dob') is-invalid @enderror" value="{{$edit->dob ?? ''}}" name="dob" type="text" placeholder="Select Date Of Birth">
                                    @error('dob')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="password">Password
                                        <span class="required-asterisk" style="color: red">*</span>
                                    </label>
                                    <input id="password" class="form-control @error('password') is-invalid @enderror password" name="password" type="password" placeholder="Password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">Confirm password
                                        <span class="required-asterisk" style="color: red"> * </span>
                                    </label>
                                    <input id="password_confirmation" class="form-control password" name="password_confirmation" type="password" placeholder="Confirm password">
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">Image
                                        <span class="required-asterisk" style="color: red"> * </span>
                                    </label>
                                    <input id="image" class="form-control password @error('image') is-invalid @enderror" name="image" type="file" placeholder="Select Image">
                                    @error('image')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    @if($edit->image)
                                        <img src="{{asset('public/storage/uploads/user-management/'.$edit->image ?? '')}}" style="height: 50px; width: 50px;">
                                    @else
                                        <img src="{{asset('public/storage/uploads/user-management/avtar.png')}}" style="height: 50px; width: 50px;">
                                    @endif

                                </div>
                                <div class="form-group">
                                    <label for="status">Status
                                        <span class="required-asterisk" style="color: red">*</span>
                                    </label>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="status" id="active" value="1" {{$edit->status === 1 ? 'checked' : ''}}> Active </label>
                                    </div>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input type="radio" class="form-check-input" name="status" id="inactive" value="0" {{$edit->status === 0 ? 'checked' : ''}}> Inactive </label>
                                    </div>
                                </div>
                                <button class="btn btn-primary" type="submit">Submit <i class="fa fa-refresh fa-spin" style="display: none" id="spinner"></i></button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{asset('public/stylehawk/validation.js')}}"></script>
    <script type="text/javascript">
        $(document).ready( function () {

            $('#dob').datepicker({
                uiLibrary: 'bootstrap4'
            });
        });
    </script>

@endsection

