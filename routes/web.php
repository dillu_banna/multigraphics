<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
|
*/
Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function(){

    Route::group(['middleware' => ['guest:admin', 'prevent-back-history']], function(){
        Route::get('/', 'LoginController@showLoginForm')->name('show.login');
        Route::post('/', 'LoginController@loginForm')->name('login');
        Route::get('forgot-password', 'ForgotPasswordController@showForgotPassword')->name('show.forgot.password');
        Route::post('forgot-password', 'ForgotPasswordController@forgotPassword')->name('forgot.password');
        Route::get('reset-password/{token}', 'RestPasswordController@showResetPassword')->name('show.reset.password');
        Route::post('reset-password', 'RestPasswordController@resetPassword')->name('reset.password');
    });

    Route::group(['middleware' => ['auth:admin', 'prevent-back-history']],function(){
        Route::get('dashboard', 'DashboardController@dashboard')->name('dashboard');
        Route::post('logout','LoginController@logout')->name('logout');
        Route::get('change-password','ChangePasswordController@showChangePassword')->name('show.change.password');
        Route::post('change-password','ChangePasswordController@changePassword')->name('change.password');
        Route::get('edit-profile','ProfileController@editProfileShow')->name('profile.edit.show');
        Route::post('edit-profile','ProfileController@editProfile')->name('profile.edit');
        
        //User Management Route
        Route::get('user-management/list', 'UserManagementController@userList')->name('user.list');
        Route::get('user-management/add', 'UserManagementController@userAddView')->name('user.add.view');
        Route::post('user-management/add', 'UserManagementController@userAdd')->name('user.add');
        Route::get('user-management/edit/{id}/{page?}', 'UserManagementController@userEditView')->name('user.edit.view');
        Route::post('user-management/edit/{id}/{page?}', 'UserManagementController@userEdit')->name('user.edit');
        Route::get('user-management/delete/{id}/{page?}', 'UserManagementController@userDelete')->name('user.delete');
        Route::get('user-management/view/{id}/{page?}', 'UserManagementController@userView')->name('user.view');
        
        
        


    });
});

