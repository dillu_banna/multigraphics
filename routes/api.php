<?php
use Illuminate\Support\Facades\Route;

Route::namespace('Api')->prefix('customer')->group(function (){
    Route::post('login','LoginController@login');
    Route::middleware('api')->group(function(){
        Route::post('change-password', 'PasswordController@changePassword');
        
    });
});
