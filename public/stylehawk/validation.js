$(document).ready(function() {
    jQuery.validator.addMethod("noSpace", function(value, element) {
        return this.optional( element ) || /^\S+$/i.test( value );
    }, "Space are not allowed");

    $.validator.addMethod("alphabetsnspace", function(value, element) {
        return this.optional(element) || /^[a-zA-Z ]*$/.test(value);
    }, "Numbers and Special Characters are not allowed");

    jQuery.validator.addMethod("numberNotStartWithZero", function(value, element) {
        return this.optional(element) || /^[1-9][0-9]+$/i.test(value);
    }, "Please enter a valid number. (Do not start with zero)");

    $.validator.addMethod("letters_numbers_special", function(value, element) {
        return this.optional(element) || /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,15}$/i.test(value);
    }, "Password should have Uppercase, lowercase, numeric and special character.");

    $.validator.addMethod("email_format", function(value, element) {
        return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})$/i.test(value);
    }, "Please enter vaild email address.");

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param * 500000)
    }, 'File size must be less than {0} MB');

    $('#admin-login-form').validate({
        rules: {
            email: {
                noSpace: true,
                required: true,
                email_format: true,
                maxlength: 100
            },
            password: {
                noSpace: true,
                required: true,
                minlength: 6,
                maxlength: 15
            }
        }
    });

    $('#email-form').validate({
        rules: {
            email: {
                noSpace: true,
                required: true,
                email_format: true,
                maxlength: 100
            }
        }

    });

    $('#reset-password').validate({
       rules: {
           password : {
               noSpace: true,
               required: true,
               minlength: 6,
               maxlength: 15
           },

           password_confirmation: {
               noSpace: true,
               required: true,
               minlength: 6,
               maxlength: 15,
               equalTo: "#password"
           }
       }
    });

    $('#change-password-form').validate({
        rules: {
            current_password: {
                noSpace: true,
                required: true,
                minlength: 6,
                maxlength: 15
            },
            password : {
                noSpace: true,
                required: true,
                minlength: 6,
                maxlength: 15
            },

            password_confirmation: {
                noSpace: true,
                required: true,
                minlength: 6,
                maxlength: 15,
                equalTo: "#password"
            }
        }
    });

    $('#edit-profile-form').validate({
        rules: {
            name: {
                required: true,
                alphabetsnspace: true,
                maxlength: 100
            },
            email: {
                noSpace: true,
                required: true,
                email: true,
                email_format: true,
                maxlength: 100
            },
            image: {
                extension: "jpg|jpeg|png",
                filesize : 50
            },
        }
    });


    $('#user-management-add-form').validate({
        rules: {
            full_name: {
                required: true,
                alphabetsnspace: true,
                maxlength: 100
            },
            dob: {
                required: true
            },
            email: {
                noSpace: true,
                required: true,
                email: true,
                email_format: true,
                maxlength: 100
            },
            phone_number: {
                noSpace: true,
                required: true,
                number: true,
                minlength: 7,
                maxlength: 15
            },
            password : {
                noSpace: true,
                required: true,
                minlength: 6,
                maxlength: 15,
                letters_numbers_special: true
            },
            password_confirmation: {
                noSpace: true,
                required: true,
                minlength: 6,
                maxlength: 15,
                equalTo: "#password"
            },
            image: {
                required: true,
                extension: "jpg|jpeg|png",
                filesize : 50
            }
        },
    });

    $('#user-management-edit-form').validate({
        rules: {
            full_name: {
                required: true,
                alphabetsnspace: true,
                maxlength: 100
            },
            dob: {
                required: true
            },
            email: {
                noSpace: true,
                required: true,
                email: true,
                email_format: true,
                maxlength: 100
            },
            phone_number: {
                noSpace: true,
                required: true,
                number: true,
                minlength: 7,
                maxlength: 15
            },
            password : {
                noSpace: true,
                minlength: 6,
                maxlength: 15,
                letters_numbers_special: true
            },
            password_confirmation: {
                noSpace: true,
                minlength: 6,
                maxlength: 15,
                equalTo: "#password"
            },
            image: {
                extension: "jpg|jpeg|png",
                filesize : 50
            }
        },
    });


});

$(document).on("submit", "form", function (e) {
    $("#spinner").show();
});
