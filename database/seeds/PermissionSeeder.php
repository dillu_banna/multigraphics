<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = [
            ['slug'=> 'manage_sub_admin', 'module_name' => 'Manage Sub Admin'],
            ['slug'=> 'user_management', 'module_name' => 'User Management'],
            ['slug'=> 'category_management', 'module_name' => 'Category Management'],
            ['slug'=> 'sub_category_management', 'module_name' => 'Sub Category Management'],
            ['slug'=> 'email_template_management', 'module_name' => 'Email Template Management'],
            ['slug'=> 'content_management', 'module_name' => 'Content Management'],
            ['slug'=> 'faq_management', 'module_name' => 'FAQ Management'],
            ['slug'=> 'introduction_screen_content', 'module_name' => 'Introduction Screen Content'],
            ['slug'=> 'global_setting_management', 'module_name' => 'Global Setting Management'],
            ['slug'=> 'marketing_mail_management', 'module_name' => 'Marketing Mail Management'],
            ['slug'=> 'offer_management', 'module_name' => 'Offer Management'],
            ['slug'=> 'review_management', 'module_name' => 'Review Management'],
            ['slug'=> 'banner_management', 'module_name' => 'Banner Management'],
            ['slug'=> 'user_reports_management', 'module_name' => 'User Reports Management'],
            ['slug'=> 'contact_us_management', 'module_name' => 'Contact Us'],
            ['slug'=> 'product', 'module_name' => 'Product'],
            ['slug'=> 'rating', 'module_name' => 'Rate & Reviews'],
        ];

        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        DB::table("permissions")->truncate();
        DB::statement("SET FOREIGN_KEY_CHECKS=1");

        DB::table('permissions')->insert($permission);
    }
}
