<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        DB::table("countries")->truncate();
        DB::statement("SET FOREIGN_KEY_CHECKS=1");

        $countries = config('countries');
        foreach ($countries as $data)
        {
            $country = new \App\Country();
            $country->sort_name = $data['sortname'];
            $country->name = $data['name'];
            $country->phone_code = $data['phonecode'];
            $country->save();
        }
    }
}
