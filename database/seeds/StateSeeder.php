<?php

use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        DB::table("states")->truncate();
        DB::statement("SET FOREIGN_KEY_CHECKS=1");

        $states = config('states');
        foreach ($states as $data)
        {
            $state = new \App\State();
            $state->name = $data['name'];
            $state->country_id = $data['country_id'];
            $state->save();
        }
    }
}
