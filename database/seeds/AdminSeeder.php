<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_data = [
            'name' => 'admin',
            'email' => 'styl@getnada.com',
            'password' => Hash::make('123456789'),
            'image' => '4X3evZGKFlSlKDqItEsrNnklNhO0jhxkhIxeRywq.jpg',
            'role' => 1,
        ];

        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        DB::table("users")->truncate();
        DB::statement("SET FOREIGN_KEY_CHECKS=1");

        if($user = User::create($admin_data))
        {
            $permission = \App\Permission::get();
            if (!empty($permission)){
                foreach ($permission as $data){
                    $permission_module = new \App\PermissionModule();
                    $permission_module->user_id = $user->id;
                    $permission_module->permission_id = $data->id;
                    $permission_module->add = 1;
                    $permission_module->edit = 1;
                    $permission_module->delete = 1;
                    $permission_module->view = 1;
                    $permission_module->save();
                }
            }
            dd('Data inserted successfully');
        }

        dd('Failed to insert');
    }
}
