<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET FOREIGN_KEY_CHECKS=0");
        DB::table("cities")->truncate();
        DB::statement("SET FOREIGN_KEY_CHECKS=1");

        $cities = config('cities');
        foreach ($cities as $data)
        {
            $city = new \App\City();
            $city->name = $data['name'];
            $city->state_id = $data['state_id'];
            $city->save();
        }
    }
}
