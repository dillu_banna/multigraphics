<?php

namespace App\Providers;

use App\GlobalSettingManagement;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('global_setting_management')){
            $global_setting = GlobalSettingManagement::select('*')->first();
            view()->share('global_setting', $global_setting);
        }

    }
}
