<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('guest:admin')->except('logout');
    }

    public function showLoginForm()
    {
        $title = 'Admin Login Form';

        return view('admin.login', compact('title'));
    }

    public function loginForm(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        $credentials = $request->only('email', 'password');

        if ($user->is_deleted === 1) {
            if (Auth::guard('admin')->attempt($credentials, $request->remember)) {
                toastr()->success('Welcome to dashboard!');

                return redirect()->route('admin.dashboard');
            }
            else {
                toastr()->error('Invalid credentials!');
                return redirect()->route('admin.show.login');
            }
        }
        toastr()->error('Your account is deleted please contact to Admin!');
        return redirect()->route('admin.show.login');
    }

    public function logout(Request $request)
    {
        Auth::guard('admin')->logout();

        toastr()->success('Logout successfully!');

        return redirect()->route('admin.show.login');
    }
}
