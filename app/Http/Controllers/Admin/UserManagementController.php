<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Country;
use App\EmailTemplate;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserManagementAddRequest;
use App\Http\Requests\UserManagementEditRequest;
use App\State;
use App\UserManagement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class UserManagementController extends Controller
{
    //Function For Fetch All Employee Record With Variation Fillter
    public function userList(Request $request)
    {
        $title = "User List";
        $user_list = UserManagement::query();
        //Start To End Date Fillter
        if (!empty($request->from) && !empty($request->to)){
            $from = Carbon::createFromFormat("m/d/Y", $request->from)->format("Y-m-d");
            $to = Carbon::createFromFormat("m/d/Y", $request->to)->format("Y-m-d");
            $user_list = $user_list->whereDate('created_at', '>=', $from)
                                    ->whereDate('created_at', '<=', $to);
            }
        //End Fillter Of Start Or End Date

        //Start Full Name Fillter
        if(!empty(trim($request->full_name))){
           $user_list = $user_list->where('full_name', 'LIKE', '%'.trim($request['full_name']).'%'); 
        }
        //End Full Name fillter

        //Start Email Fillter
        if(!empty(trim($request->email))){
            $user_list = $user_list->where('email', 'LIKE', '%'.trim($request['email']).'%'); 
         }
        //End Email Fillter
         if(!empty(trim($request->phone_number))){
            $user_list = $user_list->where('phone_number', 'LIKE', '%'.trim($request['phone_number']).'%'); 
         }
         //End Email Fillter

         //Start Status Wise Filtter 
         if(isset($request->status)){
            $user_list = $user_list->where('status', $request['status']); 
        }
        //End Status Wise Fillter
         
        //Fetch Record According To Condition
        $user_list = $user_list->orderBy('created_at', 'desc')->paginate(50);

        //Compact Data In View File
        return view("admin.user-management.list", compact('title', 'user_list'));
    }

    //Load Add Employee Form Function
    public function userAddView()
    {
        $title = "Emplyee Add Form";
        return view('admin.user-management.add', compact('title'));
    }
    //End Add Employee Form Function



    //Add Employee Record Function Start
    //Validation Checked In UserManagementAddRequest File & This File In Request Folder
    public function userAdd(UserManagementAddRequest $request)
    {
        $user_management = new UserManagement();
        $user_management->full_name = $request['full_name'];
        $user_management->phone_number = $request['phone_number'];
        $user_management->email = $request['email'];
        $user_management->dob = $request['dob'];
        $user_management->status = $request['status'];
        $user_management->password = Hash::make($request['password']);
        if ($request->hasFile('image')) {
                $file = $request->file('image');
                $destinationPath = 'public/storage/uploads/user-management/';
                $extension = $request->file('image')->getClientOriginalExtension();
                $filename =  time(). '.' . $extension;
                $file->move($destinationPath, $filename);
            $user_management->image = $filename;
            }
        $user_management->save();
        toastr()->success('User successfully added!');
        return redirect()->route('admin.user.list');
    }
    //Add Employee Record Function End

    //Load Edit Emplyee Form Start
    public function userEditView(Request $request, $id)
    {
        $title = "Emplyee Edit Form";
        $edit = UserManagement::find(base64_decode($id));
        return view('admin.user-management.edit', compact('title', 'edit'));
    }
    //Load Edit Emplyee Form End



    //Edit Employee Record Function Start
    //Validation Checked In UserManagementAddRequest File & This File In Request Folder
    public function userEdit(UserManagementEditRequest $request, $id, $page=null)
    {
        $edit = UserManagement::find(base64_decode($id));
        if ($edit)
        {
            $edit->full_name = $request['full_name'];
            $edit->phone_number = $request['phone_number'];
            $edit->email = $request['email'];
            $edit->dob = $request['dob'];
            $edit->status = $request['status'];
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $destinationPath = 'public/storage/uploads/user-management/';
                $extension = $request->file('image')->getClientOriginalExtension();
                $filename =  time(). '.' . $extension;
                $file->move($destinationPath, $filename);
                $edit->image = $filename;
            }
            if ($request->has('password'))
            {
                $edit->password = Hash::make($request['password']);
            }
            $edit->save();
            toastr()->success('User successfully updated!');
            return redirect('admin/user-management/list?page='.$page);
        }
        else{
            toastr()->error('Something went wrong!');
            return redirect()->route('admin/user-management/list?page='.$page);
        }
    }
    //Edit Employee Record Function End

    //Record delete function
    public function userDelete(Request $request, $id, $page=null)
    {
        $delete = UserManagement::findOrFail(base64_decode($id));

        //If id received in valid then checked if condition otherwise checked else condition
        if ($delete)
        {
            $delete->delete();
            toastr()->success('User successfully deleted!');
            return redirect('admin/user-management/list?page='.$page);
        }
        else{
            toastr()->error('Something went wrong!');
            return redirect('admin/user-management/list?page='.$page);
        }

    }


}
