<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserManagementAddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|regex:/^[a-zA-Z\s]+$/|max:100',
            'phone_number' => 'required|digits_between:7,15',
            'email' => 'required|email|regex:/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})$/|unique:user_management|max:100',
            'dob' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg',
            'status' => 'required|in:0,1',
            'password' => 'required|string|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/|min:6|max:15|confirmed',


        ];
    }

    public function messages()
    {
        return[
            'password.regex' => 'Password should have Uppercase, lowercase, numeric and special character.'
        ];
    }
}
