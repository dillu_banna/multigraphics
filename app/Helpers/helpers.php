<?php

if (!function_exists('permissionModule'))
{
    function permissionModule()
    {
        $permission = \App\PermissionModule::with('permission')
                                            ->where('user_id', auth('admin')->user()->id)->get();
        return $permission;

    }
}
