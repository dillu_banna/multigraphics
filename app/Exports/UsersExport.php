<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class UsersExport implements FromCollection, WithHeadings
{
    private $data_user;

    public function __construct($excel_data){
        $this->data_user = $excel_data;
    }

    public function collection()
    {
        $user_data = [];

        if (!empty($this->data_user)){
            foreach ($this->data_user as $key => $export_data) {
                $user_data[] = [
                    'S.No' => ++$key,
                    'first_name' => ucfirst($export_data->first_name),
                    'last_name' => ucfirst($export_data->last_name),
                    'user_name' => $export_data->user_name,
                    'phone_number' => $export_data->phone_number,
                    'email' => $export_data->email,
                    'country_id' => ucfirst($export_data->countries->name),
                    'state_id' => ucfirst($export_data->states->name),
                    'city_id' => ucfirst($export_data->cities->name),
                    'zipcode' => $export_data->zipcode,
                    'status' => $export_data->status ? 'Active' : 'Inactive',
                    'Date' => date_format($export_data->created_at ?? '', 'Y/m/d'),
                ];
            }
        }
        return collect($user_data);
    }

    public function headings(): array
    {
        return [
            'S.No',
            'First Name',
            'Last Name',
            'User Name',
            'Phone Number',
            'Email',
            'Country',
            'State',
            'City',
            'Zipcode',
            'Status',
            'Date'
        ];
    }
}
