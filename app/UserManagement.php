<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class UserManagement extends Model
{
    //use HasApiTokens, Notifiable;

    protected $guarded = [];

    protected $hidden = ['password'];

    public function countries()
    {
        return $this->belongsTo(Country::class,'country_id', 'id');
    }

    public function cities()
    {
        return $this->belongsTo(City::class,'city_id', 'id');
    }

    public function states()
    {
        return $this->belongsTo(State::class,'state_id', 'id');
    }

    public function shippingAddress(){
        return $this->hasOne(ShippingAddress::class,'user_id', 'id');
    }

    public function billingAddress(){
        return $this->hasOne(BillingAddress::class, 'user_id', 'id');
    }


}
